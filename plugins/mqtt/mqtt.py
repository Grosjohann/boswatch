#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
MQTT-Plugin to send FMS-, ZVEI- and POCSAG - messages to a MQTT-Broker
@author: Marco Grosjohann
@requires: MQTT-Configuration has to be set in the config.ini and library paho-mqtt is required
Installation: 
    sudo apt-get install python-pip
    sudo apt-get install python3-pip

    sudo pip install paho-mqtt (Python 2.x)
    sudo pip3 install paho-mqtt (Python 3.x)
"""

import logging  # Global logger
import paho.mqtt.client as mqtt #import the client
from includes import globalVars  # Global variables

# from includes.helper import timeHandler
from includes.helper import configHandler
from includes.helper import wildcardHandler

def isSignal(poc_id):
    """
    @type    poc_id: string
    @param   poc_id: POCSAG Ric

    @requires:  Configuration has to be set in the config.ini

    @return:    True if the Ric is Signal, other False
    @exception: none
    """
    # If RIC is Signal return True, else False
    if globalVars.config.get("POC", "netIdent_ric"):
        if poc_id in globalVars.config.get("POC", "netIdent_ric"):
            logging.info("RIC %s is net ident", poc_id)
            return True
        else:
            logging.info("RIC %s is no net ident", poc_id)
            return False

def onLog(client, userdata, level, buf):
    logging.debug("buffer: %s", buf)

def onConnect(client, userdata, flags, rc):
    logging.debug("Client connected")

def onDisconnect(client, userdata, rc):
    logging.debug("Client disconnected")

##
#
# onLoad (init) function of plugin
# will be called one time by the pluginLoader on start
#
def onLoad():
    """
    While loading the plugins by pluginLoader.loadPlugins()
    this onLoad() routine is called one time for initialize the plugin
    @requires:  nothing
    @return:    nothing
    """
    # nothing to do for this plugin
    return


##
#
# Main function of Divera-plugin
# will be called by the alarmHandler
#
def run(typ, freq, data):
    """
    This function is the implementation of the Divera-Plugin.
    It will send the data to Divera API
    @type    typ:  string (FMS|ZVEI|POC)
    @param   typ:  Typ of the dataset
    @type    data: map of data (structure see readme.md in plugin folder)
    @param   data: Contains the parameter
    @type    freq: string
    @keyword freq: frequency of the SDR Stick
    @requires:  Divera-Configuration has to be set in the config.ini
    @return:    nothing
    """
    try:
        if configHandler.checkConfig("mqtt"):  # read and debug the config

            if typ == "FMS":
                #
                # building message for FMS
                #
                message = globalVars.config.get("mqtt", "fms_message")

            elif typ == "ZVEI":
                #
                # building message for ZVEI
                #
                message = globalVars.config.get("mqtt", "zvei_message")

            elif typ == "POC":
                if isSignal(data["ric"]):
                    return
                #
                # building message for POC
                #
                message = globalVars.config.get("mqtt", "poc_message")

            else:
                logging.warning("Invalid type: %s", typ)
                return

            try:
                #
                # MQTT-Request
                #
                logging.debug("send MQTT for %s", typ)

                # replace the wildcards
                message = wildcardHandler.replaceWildcards(message, data)
                topic = globalVars.config.get("mqtt", "topic")

                # Logging data to send
                logging.debug("Topic   : %s", topic)
                logging.debug("Message : %s", message)

                # create Client
                client = mqtt.Client(globalVars.config.get("mqtt", "client_name"))
                client.on_log = onLog
                client.on_connect = onConnect
                client.on_disconnect = onDisconnect
                
                # Username / Passwd
                use_userpasswd = True
                username = globalVars.config.get("mqtt", "username")
                passwd = globalVars.config.get("mqtt", "passwd")
                
                if not username:
                    logging.warning("The username is not given")
                    use_userpasswd = False
                if not passwd:
                    logging.warning("The passwd is not given")
                    use_userpasswd = False
                    
                if use_userpasswd == True:
                    client.username_pw_set(username,passwd)
                else:
                    logging.info("MQTT without username/passwd")
                                
                # Certification
                use_certification = True
                ca_crt = globalVars.config.get("mqtt", "ca_crt")
                client_crt = globalVars.config.get("mqtt", "client_crt")
                client_key = globalVars.config.get("mqtt", "client_key")
                
                if not ca_crt:
                    logging.warning("The Certificate authority certificate (ca.crt) is not given")
                    use_certification = False
                if not client_crt:
                    logging.warning("The client certifcate file (client.crt) is not given")
                    use_certification = False
                if not client_key:
                    logging.warning("The client private key (client.key) is not given")
                    use_certification = False

                if use_certification == True:
                    client.tls_set(ca_crt, client_crt, client_key)
                else:
                    logging.info("MQTT without Certification")
                
                # start the connection
                host = globalVars.config.get("mqtt", "broker_address")
                port = globalVars.config.get("mqtt", "broker_port")
                if not port:
                    port = 1883
                client.connect(host, port)

                # Publish
                client.publish(topic, message)

            except:
                logging.error("Error sending Data to MQTT")
                logging.debug("Error sending Data to MQTT", exc_info=True)

    except:
        logging.error("unknown error")
        logging.debug("unknown error", exc_info=True)
